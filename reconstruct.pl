#!/usr/bin/perl

use strict;
use warnings;
use diagnostics;
use Data::Dumper;
$Data::Dumper::Indent = 1;
use feature qw(say signatures);
no warnings "experimental::signatures";
use List::Util qw(min max);
use File::Find;
use File::Path qw(make_path);
use File::Copy;
use Date::Parse;
use Cwd;

my $domain = $ARGV[0];
my %files;
my %dates;

sub walker {
	return if -d $_;

	my ($date, $time, $origfname) = $_ =~ /^(\d{8})(\d{6})-(.*)/;

	my $basedir = substr($File::Find::dir, length("out/$domain/$domain-rewrite"));
	say "$basedir $_ $origfname";

	my $path_and_fname = "$basedir/$_";
	my $orig_path_and_fname = "$basedir/$origfname";
	$orig_path_and_fname =~ s/^[^\/]*//;

	$dates{$date} = 1;
	if($files{$orig_path_and_fname}->{$date})
	{
		$files{$orig_path_and_fname}->{$date} = max($files{$orig_path_and_fname}->{$date}, $path_and_fname);
	}
	else
	{
		$files{$orig_path_and_fname}->{$date} = $path_and_fname;
	}

}

find(\&walker, "out/$domain/$domain-rewrite/");
print Dumper(\%files);
say scalar(keys %dates);

my %outputs;
my @all_dates = sort keys %dates;
foreach my $date (@all_dates)
{
	foreach my $file (keys %files)
	{
		my @file_dates = sort { abs(str2time($a) - str2time($date)) <=> abs(str2time($b) - str2time($date)) } keys %{$files{$file}};
		#say join(" ", keys %{$files{$file}});
		#say join(" ", map { abs(str2time($_) - str2time($date)) } %{$files{$file}});
		#say join(" ", @file_dates);
		my $closest_snapshot = $files{$file}->{$file_dates[0]};
		$outputs{$date}->{$file} = $closest_snapshot;
	}
}

sub hash_date_output
{
	my ($date) = @_;
	return join(",", map { $_ . "=" . $outputs{$date}->{$_} } sort keys %{$outputs{$date}});
}

my %redundant_dates;
foreach my $i (1 .. $#all_dates - 1)
{
	say $i, " ", $all_dates[$i];
	$redundant_dates{$all_dates[$i]} = 1 if
		hash_date_output($all_dates[$i]) eq hash_date_output($all_dates[$i - 1]) &&
		hash_date_output($all_dates[$i]) eq hash_date_output($all_dates[$i + 1]);
	say "-- $all_dates[$i] ".hash_date_output($all_dates[$i]);
}

say "redundant dates:";
say Dumper(\%redundant_dates);

foreach my $date (@all_dates)
{
	next if($redundant_dates{$date});

	foreach my $file (keys %files)
	{
		my $out_file = "out/$domain/$date/$file";
		my ($out_path) = $out_file =~ /(.*)\//;

		my $closest_snapshot = $outputs{$date}->{$file};
		my $symlink_target = getcwd() . "/out/$domain/$domain-rewrite/$closest_snapshot";

		#say "$out_file <= $closest_snapshot";
		say "$out_file -> $symlink_target";
		make_path($out_path);
		#copy($closest_snapshot, $out_file);
		#link($closest_snapshot, $out_file);
		symlink($symlink_target, $out_file);
	}
	#last;
}

print "\n";

