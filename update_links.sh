#!/bin/bash
set -eu

if [[ $# -ne 1 ]]
then
	echo "Usage: $0 domain"
	exit
fi

domain="$1"
SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

cd out/"$domain"/"$domain"
find -type d -exec mkdir -p ../"$domain-rewrite"/{} \;

find -type f | while read f
do
	"$SCRIPT_DIR"/link_rewriter.pl "$domain" "$f" > ../"$domain-rewrite/$f"
done
