#!/usr/bin/perl

use strict;
use warnings;
use diagnostics;
use Data::Dumper;
use feature qw(say signatures);
no warnings "experimental::signatures";
use List::Util qw(min max);

my $domain = shift;

while(<>)
{
	s/\b((?:href|src|URL)=["']?)(?:https?:)?\/\/(?:www\.)?\Q$domain\E\//$1\//g;

	# this pattern is used by CrikeyCon to redirect to https
	if(index($_, 'window.location.href = "https:" + window.location.href') >= 0)
	{
		$_ = "while(0){\n  // removed by link_rewriter\n  $_\n}";
	}
	print;
}
