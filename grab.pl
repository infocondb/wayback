#!/usr/bin/perl

use strict;
use warnings;
use diagnostics;
use Data::Dumper;
use feature qw(say signatures);
no warnings "experimental::signatures";
use List::Util qw(min max);
use LWP::Simple;
use JSON;
use File::Path qw(make_path);

my $PRETEND = 0;

if(scalar(@ARGV) != 1)
{
	say "Usage: $0 domain";
	exit 1;
}
my $domain = $ARGV[0];
mkdir("out/$domain") if(!$PRETEND);
#my $cdx_content = get("http://web.archive.org/cdx/search/cdx?url=${domain}&matchType=prefix&filter=statuscode:200&limit=100000&output=json");
my $cdx_content = get("http://web.archive.org/cdx/search/cdx?url=${domain}&matchType=prefix&limit=100000&output=json");
open(my $fh, '>', "out/$domain/$domain.cdx.json");
print $fh $cdx_content;
close $fh;
my $cdx = decode_json($cdx_content);
shift @$cdx;
print Dumper($cdx);
my $count = scalar(@$cdx);
my $i = 0;
foreach my $entry (@$cdx)
{
	my ($urlkey, $timestamp, $original, $mimetype, $statuscode, $digest, $length) = @$entry;
	$i++;
	next unless($statuscode ne "-" && $statuscode == 200);
	my $url = "https://web.archive.org/web/${timestamp}im_/$original";
	my $original_clean = $original;
	$original_clean =~ s/^https?:\/\///;
	$original_clean = "$original_clean/" if($original_clean =~ /^[^\/]*$/);
	$original_clean =~ s/^www\.//;
	$original_clean =~ s/^([^:]*)/\L$1/;
	$original_clean =~ s/^([^\/]*):80\//$1\//;
	$original_clean =~ s/\?$//;
	if($original_clean =~ /\/[^.\/]+$/)
	{
		$original_clean .= "/";
	}
	$original_clean =~ s/\/$/\/index.html/;

	my ($original_path, $original_file) = $original_clean =~ /(.*)\/([^\/]*)$/;
	my $out = "out/$domain/$original_path/$timestamp-$original_file";
	my ($out_path) = $out =~ /(.*)\/[^\/]*$/;

	#next unless $out =~ /20130620175212/;
	#say "original: $original";
	#say "original_clean: $original_clean";
	#say "original_path: $original_file";
	say "[$i/$count] $urlkey  -  $original ($url) => $out";

	if($out =~ /\.(mp4|m4v)(\?[^\/]*)?$/)
	{
		say "SKIPPING mp4!";
		next;
	}

	if(!$PRETEND)
	{
		make_path($out_path);
		if( ! -e $out)
		{
			my $code = getstore($url, $out);
			if($code != 200)
			{
				say $code;
			}
		}
	}
}
