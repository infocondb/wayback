from http.server import HTTPServer, SimpleHTTPRequestHandler
import os
from os import path
import sys
import re
import glob
import threading
import posixpath

domain = None
dates = []
previous_date_idx = 0
current_date_idx = 0
case_insensitive_path_mapping = dict()

def get_base_path(domain: str, date_idx: int) -> str:
    return f"out/{domain}/{dates[date_idx]}/"

class Handler(SimpleHTTPRequestHandler):

    def do_GET(self):
        #print(self.__dict__)
        return super().do_GET()

    def translate_path(self, path):
        orig_path = path
        if path.startswith("/"):
            path = path[1:]
        path_translated = get_base_path(domain, current_date_idx) + path.replace("?", "%3f")
        if True:
            path_translated = case_insensitive_path_mapping.get(path_translated.lower(), path_translated)
        print(f"{orig_path} -> {path_translated}")
        return super().translate_path(path_translated)

    def guess_type(self, path):
        print(f"guess_type: {path}")
        head, tail = posixpath.split(path)
        path = posixpath.join(head, tail.split("?", 1)[0])
        print(f"guess_type: {path}")
        return super().guess_type(path)

def create_server():
    ip = '0.0.0.0'
    port = 8081
    server_address = (ip, port)
    httpd = HTTPServer(server_address, Handler)
    print(f"Running server... http://{ip}:{port}/")
    httpd.serve_forever()

if __name__ == "__main__":
    domain = sys.argv[1]
    print(f"Domain: {domain}")

    for file in os.listdir(f"out/{domain}"):
        if re.match("\d{8}", file):
            dates.append(file)
    dates.sort()

    for root, dirs, files in os.walk(f"out/{domain}"):
        for name in files:
            path = os.path.join(root, name)
            case_insensitive_path_mapping[path.lower()] = path

    threading.Thread(target=create_server).start()

    while True:
        i = input(f"\n[{current_date_idx+1}/{len(dates)+1}] {dates[current_date_idx][0:4]}-{dates[current_date_idx][4:6]}-{dates[current_date_idx][6:8]}: ")
        new_date_idx = current_date_idx

        delta = 1
        if len(i) > 1:
            delta = int(i[0:-1])

        if i.endswith("h"):
            new_date_idx = (new_date_idx - delta + len(dates)) % len(dates)
        elif i.endswith("l"):
            new_date_idx = (new_date_idx + delta) % len(dates)
        elif i.endswith("G"):
            new_date_idx = delta

        elif i == "d":
            os.system(f"diff -r --unified=3 {get_base_path(domain, previous_date_idx)} {get_base_path(domain, current_date_idx)} | ./diff-format.pl")
            print()

        elif i == "f":
            current_date_int = int(dates[current_date_idx])
            for dirpath, dirnames, filenames in os.walk(get_base_path(domain, current_date_idx)):
                for filename in filenames:
                    real = path.realpath(path.join(dirpath, filename))
                    pathname, basename = path.split(real)
                    fileglob = path.join(pathname, f"??????????????{basename[14:]}")
                    matching_files = glob.glob(fileglob)
                    for matching_file in matching_files:
                        if int(path.basename(matching_file)[0:8]) <= current_date_int:
                            print(path.join(dirpath, filename))
                            break

        if new_date_idx != current_date_idx:
            previous_date_idx, current_date_idx = current_date_idx, new_date_idx
            os.system(f"diff -qr {get_base_path(domain, previous_date_idx)} {get_base_path(domain, current_date_idx)}")

