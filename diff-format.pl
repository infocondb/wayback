#!/usr/bin/perl

use strict;
use warnings;
use diagnostics;
use Data::Dumper;
use feature qw(say signatures);
no warnings "experimental::signatures";
use List::Util qw(min max);

my $width = `tput cols`;
my $num_width = 3;
my $gutter_padding = 1;
my $pane_width = int(($width - (1+$gutter_padding*2)) / 2) - ($num_width + 1);

my $left_fname;
my $right_fname;
my $left_line;
my $right_line;

while(<>)
{
	chomp;
	s/\r$//;
	if(/^diff /)
	{
		# nothing
	}
	elsif(/^--- (.+?)	/)
	{
		$left_fname = $1;
	}
	elsif(/^\+\+\+ (.+?)	/)
	{
		$right_fname = $1;
		print
			"="x($num_width + 1),
			sprintf("%-${pane_width}s", " $left_fname"),
			" " x ($gutter_padding * 2 + 1),
			"="x($num_width + 1),
			sprintf("%-${pane_width}s", " $right_fname"),
			"\n";
	}
	elsif(/^@@ -(\d+),\d+ \+(\d+),\d+ @@/)
	{
		$left_line = $1;
		$right_line = $2;
		print "-"x$width, "\n";
	}
	elsif(/^\+(.*)/)
	{
		my $content = $1;
		print
			" " x ($num_width + 1 + $pane_width + $gutter_padding),
			">",
			" " x $gutter_padding,
			sprintf("%-${num_width}d ", $right_line),
			sprintf("%-${pane_width}s", $content),
			"\n";
		$right_line++;
	}
	elsif(/^-(.*)/)
	{
		my $content = $1;
		print 
			sprintf("%-${num_width}d ", $left_line),
			sprintf("%-${pane_width}s", $content),
			" " x $gutter_padding,
			"<",
			" " x ($gutter_padding + $num_width + 1 + $pane_width),
			"\n";
		$left_line++;
	}
	elsif(/^ (.*)/)
	{
		my $content = $1;
		print 
			sprintf("%-${num_width}d ", $left_line),
			sprintf("%-${pane_width}s", $content),
			" " x $gutter_padding,
			"|",
			" " x $gutter_padding,
			sprintf("%-${num_width}d ", $right_line),
			sprintf("%-${pane_width}s", $content),
			"\n";
		$left_line++;
		$right_line++;
	}
}

