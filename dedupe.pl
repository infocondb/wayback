#!/usr/bin/perl

use strict;
use warnings;
use diagnostics;
use Data::Dumper;
use feature qw(say signatures);
no warnings "experimental::signatures";
use List::Util qw(min max);
use File::Find;
use Digest::MD5;

my $domain = $ARGV[0];
my %file_hashes;

sub walker {
	my ($ts, $origfname) = $_ =~ /^(\d+)-(.*)/;

	return if -d $_;

	open (my $fh, "<", $_) or die "Can't open $_";
	binmode $fh;
	my $md5 = Digest::MD5->new->addfile($fh)->hexdigest;

	$file_hashes{$File::Find::dir . "/" . $origfname}->{$ts} = {md5 => $md5, fullpath => "$File::Find::dir/$_"};

	say "$File::Find::dir $_ $ts $origfname $md5";
}

find(\&walker, "out/$domain");

print "\n";

while (my ($f, $file_data) = each %file_hashes)
{
	my @times = sort keys %{$file_data};

	say "keep " . $file_data->{$times[0]}->{fullpath};
	for (my $i = 1; $i < scalar(@times) - 1; $i++)
	{
		if(
			$file_data->{$times[$i]}->{md5} eq $file_data->{$times[$i-1]}->{md5} &&
			$file_data->{$times[$i]}->{md5} eq $file_data->{$times[$i+1]}->{md5}
		) {
			say "del  " . $file_data->{$times[$i]}->{fullpath};
			unlink($file_data->{$times[$i]}->{fullpath});
		}
		else
		{
			say "keep " . $file_data->{$times[$i]}->{fullpath};
		}
	}
	say "keep " . $file_data->{$times[-1]}->{fullpath};
}
