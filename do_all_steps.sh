#!/bin/bash

if [[ $# -ne 1 ]]
then
	echo "Usage: $0 domain"
	exit 1
fi

domain=$1

./grab.pl "$domain"
./dedupe.pl "$domain"
./update_links.sh "$domain"
./reconstruct.pl "$domain"
